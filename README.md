# FilesManager Backend

## Overview

A simple backend server for managing files built with express.js

## Features

- **RESTful API**: Well-defined and documented RESTful API endpoints for creating, reading, updating, and deleting files.

## Installation

1. Clone the repository: `git clone https://gitlab.com/oleksandryanov/express-first.git`
2. Navigate to the project directory: `cd express-first`
3. Install dependencies: `npm install`
5. Start the server: `npm start`

## API Endpoints

- **POST /api/files**: Create a file.
- **GET /api/files**: Get files.
- **GET /api/files/:filename**: Get file by name.
- **PUT /api/files/:filename**: Update file by name.
- **DELETE /api/files/:filename**: Delete file by name.

## Contributing

Contributions are welcome! If you have any suggestions, bug fixes, or new features to add, please feel free to open an issue or submit a pull request.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Contact

For any inquiries or feedback, please contact me via email: oleksandr.yanov.eu@gmail.com.
